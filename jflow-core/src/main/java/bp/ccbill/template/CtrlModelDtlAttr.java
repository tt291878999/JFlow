package bp.ccbill.template;

import bp.da.*;
import bp.en.*;
import bp.sys.*;
import bp.wf.template.*;
import bp.wf.*;
import bp.ccbill.*;
import java.util.*;

/** 
  控制模型-属性
*/
public class CtrlModelDtlAttr extends EntityMyPKAttr
{
	/** 
	 表单ID
	*/
	public static final String FrmID = "FrmID";
	/** 
	 控制类型
	*/
	public static final String CtrlObj = "CtrlObj";
	/** 
	 组织类型 Station,Dept,User
	*/
	public static final String OrgType = "OrgType";
	/** 
	 IDs
	*/
	public static final String IDs = "IDs";
}